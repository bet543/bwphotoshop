const photoshopConnection = require('photoshop-connection')
var fs = require('fs');
var exec = require('child_process').exec;
var textureConf = null;
var isFinish = false;
var execFinishCnt = 0;
var execCnt = 0;

if (!fs.existsSync("packerList")) {
    fs.mkdirSync("packerList");
}

fs.readFile("config.json", 'utf8', function (err, content) {
    if (err) {
        console.log(err);
        return;
    }
    else {
        var conf = JSON.parse(content);
        textureConf = conf.texturePacker;
        photoshopConnection.createClient(conf.psConInfo).then((photoshopClient) => {
            var jsPath = `${__dirname}\\Auto.jsx`;
            // "var SCRIPTS_FOLDER =  decodeURI(app.path + '/' + localize("$$$/ScriptingSupport/InstalledScripts=Presets/Scripts"));
            // var geo_dynamic = File("C:/Users/User/Desktop/photoshop_script/Auto.jsx");
            // $.evalFile (geo_dynamic);"
            photoshopClient.sendCommand(`var SCRIPTS_FOLDER =  decodeURI(app.path + '/' + localize("$$$/ScriptingSupport/InstalledScripts=Presets/Scripts"));
            var geo_dynamic = File("${jsPath.replace(/\\/g, "/")}");
            $.evalFile (geo_dynamic);`);
        }).catch((err) => {
            console.log(`Connection Error: ${err}`);
        });
    }
});
function texturePackerTimer(content) {
    fs.readdirSync("./packerList").forEach(file => {
        console.log(file);
        var prefix = decodeURI(file.substring(0, file.indexOf(".")));
        var array = fs.readFileSync("./packerList/" + file).toString().split("\n");
        var path = `${__dirname}\\${prefix}\\`;
        // return;
        for (let i = 0; i < array.length; i++) {
            let item = array[i];
            console.log(item);
            var fileName = item.trim();
            if (fileName == "") {
                break;
            }
            else if (fileName == "#end") {
                isFinish = true;
                break;
            }
            let cli =
                '"' + textureConf.cmd + '"' +
                ' --data ' + (prefix + "/" + fileName + ".json") +
                ' --force-publish' +
                ' --format ' + textureConf.dataFormat +
                ' --sheet ' + (fileName + ".png") +
                ' --texture-format ' + textureConf.textureFormat +
                ' --max-width ' + textureConf.maxWidth +
                ' --max-height ' + textureConf.maxHeight +
                ' --trim-mode None ' +
                ' --extrude 0 ' +
                ' --disable-auto-alias ' + path + fileName +
                ' --png-opt-level 0 ' +
                ' --algorithm Basic ' +
                ' --size-constraints AnySize' +
                '';
            console.log("cli:", cli);
            execCnt++;
            exec(cli, { encoding: 'utf8' }, function (err, stdout, stderr) {
                execFinishCnt++;
                if (err) {
                    // console.log(err);
                    console.log('\n');
                    console.log("The CLI lead to error is: ", cli);
                    console.log('\n');
                    return;
                }
                console.log('stdout:' + stdout);
                console.log('stderr:' + stderr);
                //-2 because "#end" and "\n"
                if (isFinish && execFinishCnt == execCnt) {
                    process.exit();
                }
            });
        }
        fs.unlinkSync("./packerList/" + file);
    })
    if (isFinish) {
        process.exit();
        return;
    }
    setTimeout(texturePackerTimer, 5000);
}