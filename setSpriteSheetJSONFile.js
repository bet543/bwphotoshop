var fs = require('fs');
var col = 6;
var row = 8;
var w = 150, h = 150, interval = 0;
var fileName = "Symbol_Hourse";
var startX = 0;
var startY = 0;
var data = {
	frames: {},
	meta: {
		"app": "KentScript",
		"version": "1.0",
		"image": fileName + ".png",
		"format": "RGBA8888",
		"size": {
			"w": 900,
			"h": 1200
		},
		"scale": "1",
	}
};
for (var i = 0; i < row; i++) {
	for (var j = 0; j < col; j++) {
		data.frames[fileName + "_" + padLeft((i * col + j).toString(), 2) + ".png"] = {
			"frame": {
				"x": j * w + interval * j + startX,
				"y": i * h + interval * i + startY,
				"w": w,
				"h": h
			},
			"rotated": false,
			"trimmed": false,
			"spriteSourceSize": {
				"x": 0,
				"y": 0,
				"w": w,
				"h": h
			},
			"sourceSize": {
				"w": w,
				"h": h
			},
			"pivot": {
				"x": 0.5,
				"y": 0.5
			}
		}
	}
}

function padLeft(str, length) {
	if (str.length >= length) {
		return str;
	}
	else {
		return padLeft("0" + str, length);
	}
}


let json = JSON.stringify(data); //convert it back to json
fs.writeFile(fileName + '.json', json, 'utf8'); // write it back 