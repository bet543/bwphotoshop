﻿// Copyright 2007.  Adobe Systems, Incorporated.  All rights reserved.
// This script will export each layer in the document to a separate file.
// Written by Naoki Hada
// ZStrings and auto layout by Tom Ruark

/*
@@@BUILDINFO@@@ Export Layers To Files.jsx 1.0.0.16
*/

/*

// BEGIN__HARVEST_EXCEPTION_ZSTRING

<javascriptresource>
<name>$$$/JavaScripts/ExportLayersToFiles/Menu=Export Layers to Files...</name>
<category>layers</category>
<enableinfo>true</enableinfo>
<eventid>6f1c2cf5-4a97-4e32-8f59-f5d7a087adef</eventid>
</javascriptresource>

// END__HARVEST_EXCEPTION_ZSTRING

*/

/* jshint ignore:start */
/**
 * Date.now - from: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/now
 */
Date.now || (Date.now = function () { return (new Date).getTime() });
/**
 * Date.toISOString - from: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toISOString
 */
Date.prototype.toISOString || !function () { function pad(number) { return number < 10 ? "0" + number : number } Date.prototype.toISOString = function () { return this.getUTCFullYear() + "-" + pad(this.getUTCMonth() + 1) + "-" + pad(this.getUTCDate()) + "T" + pad(this.getUTCHours()) + ":" + pad(this.getUTCMinutes()) + ":" + pad(this.getUTCSeconds()) + "." + (this.getUTCMilliseconds() / 1e3).toFixed(3).slice(2, 5) + "Z" } }();
/**
* JSON - from: https://github.com/douglascrockford/JSON-js
*/
if (typeof JSON !== 'object') { JSON = {}; } (function () { 'use strict'; function f(n) { return n < 10 ? '0' + n : n; } function this_value() { return this.valueOf(); } if (typeof Date.prototype.toJSON !== 'function') { Date.prototype.toJSON = function () { return isFinite(this.valueOf()) ? this.getUTCFullYear() + '-' + f(this.getUTCMonth() + 1) + '-' + f(this.getUTCDate()) + 'T' + f(this.getUTCHours()) + ':' + f(this.getUTCMinutes()) + ':' + f(this.getUTCSeconds()) + 'Z' : null; }; Boolean.prototype.toJSON = this_value; Number.prototype.toJSON = this_value; String.prototype.toJSON = this_value; } var cx, escapable, gap, indent, meta, rep; function quote(string) { escapable.lastIndex = 0; return escapable.test(string) ? '"' + string.replace(escapable, function (a) { var c = meta[a]; return typeof c === 'string' ? c : '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4); }) + '"' : '"' + string + '"'; } function str(key, holder) { var i, k, v, length, mind = gap, partial, value = holder[key]; if (value && typeof value === 'object' && typeof value.toJSON === 'function') { value = value.toJSON(key); } if (typeof rep === 'function') { value = rep.call(holder, key, value); } switch (typeof value) { case 'string': return quote(value); case 'number': return isFinite(value) ? String(value) : 'null'; case 'boolean': case 'null': return String(value); case 'object': if (!value) { return 'null'; } gap += indent; partial = []; if (Object.prototype.toString.apply(value) === '[object Array]') { length = value.length; for (i = 0; i < length; i += 1) { partial[i] = str(i, value) || 'null'; } v = partial.length === 0 ? '[]' : gap ? '[\n' + gap + partial.join(',\n' + gap) + '\n' + mind + ']' : '[' + partial.join(',') + ']'; gap = mind; return v; } if (rep && typeof rep === 'object') { length = rep.length; for (i = 0; i < length; i += 1) { if (typeof rep[i] === 'string') { k = rep[i]; v = str(k, value); if (v) { partial.push(quote(k) + (gap ? ': ' : ':') + v); } } } } else { for (k in value) { if (Object.prototype.hasOwnProperty.call(value, k)) { v = str(k, value); if (v) { partial.push(quote(k) + (gap ? ': ' : ':') + v); } } } } v = partial.length === 0 ? '{}' : gap ? '{\n' + gap + partial.join(',\n' + gap) + '\n' + mind + '}' : '{' + partial.join(',') + '}'; gap = mind; return v; } } if (typeof JSON.stringify !== 'function') { escapable = /[\\\"\u0000-\u001f\u007f-\u009f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g; meta = { '\b': '\\b', '\t': '\\t', '\n': '\\n', '\f': '\\f', '\r': '\\r', '"': '\\"', '\\': '\\\\' }; JSON.stringify = function (value, replacer, space) { var i; gap = ''; indent = ''; if (typeof space === 'number') { for (i = 0; i < space; i += 1) { indent += ' '; } } else if (typeof space === 'string') { indent = space; } rep = replacer; if (replacer && typeof replacer !== 'function' && (typeof replacer !== 'object' || typeof replacer.length !== 'number')) { throw new Error('JSON.stringify'); } return str('', { '': value }); }; } if (typeof JSON.parse !== 'function') { cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g; JSON.parse = function (text, reviver) { var j; function walk(holder, key) { var k, v, value = holder[key]; if (value && typeof value === 'object') { for (k in value) { if (Object.prototype.hasOwnProperty.call(value, k)) { v = walk(value, k); if (v !== undefined) { value[k] = v; } else { delete value[k]; } } } } return reviver.call(holder, key, value); } text = String(text); cx.lastIndex = 0; if (cx.test(text)) { text = text.replace(cx, function (a) { return '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4); }); } if (/^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) { j = eval('(' + text + ')'); return typeof reviver === 'function' ? walk({ '': j }, '') : j; } throw new SyntaxError('JSON.parse'); }; } }());
/**
 * Object.create - from: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/create
 */
"function" != typeof Object.create && (Object.create = function (undefined) { var Temp = function () { }; return function (prototype, propertiesObject) { if (prototype !== Object(prototype) && null !== prototype) throw TypeError("Argument must be an object, or null"); Temp.prototype = prototype || {}, propertiesObject !== undefined && Object.defineProperties(Temp.prototype, propertiesObject); var result = new Temp; return Temp.prototype = null, null === prototype && (result.__proto__ = null), result } }());
/**
* Array.forEach - from: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach
*/
Array.prototype.forEach || (Array.prototype.forEach = function (r, t) { var o, n; if (null == this) throw new TypeError(" this is null or not defined"); var e = Object(this), i = e.length >>> 0; if ("function" != typeof r) throw new TypeError(r + " is not a function"); for (arguments.length > 1 && (o = t), n = 0; i > n;) { var a; n in e && (a = e[n], r.call(o, a, n, e)), n++ } });
/**
 * Array.isArray - from: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/isArray
 */
Array.isArray || (Array.isArray = function (arg) { return "[object Array]" === Object.prototype.toString.call(arg) });
/**
 * Array.every - from: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/every
 */
Array.prototype.every || (Array.prototype.every = function (callbackfn, thisArg) { "use strict"; var T, k; if (null == this) throw new TypeError("this is null or not defined"); var O = Object(this), len = O.length >>> 0; if ("function" != typeof callbackfn) throw new TypeError; for (arguments.length > 1 && (T = thisArg), k = 0; k < len;) { var kValue; if (k in O) { kValue = O[k]; var testResult = callbackfn.call(T, kValue, k, O); if (!testResult) return !1 } k++ } return !0 });
/**
 * Array.filter - from: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter
 */
Array.prototype.filter || (Array.prototype.filter = function (fun) { "use strict"; if (void 0 === this || null === this) throw new TypeError; var t = Object(this), len = t.length >>> 0; if ("function" != typeof fun) throw new TypeError; for (var res = [], thisArg = arguments.length >= 2 ? arguments[1] : void 0, i = 0; i < len; i++)if (i in t) { var val = t[i]; fun.call(thisArg, val, i, t) && res.push(val) } return res });
/**
 * Array.indexOf - from: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/indexOf
 */
Array.prototype.indexOf || (Array.prototype.indexOf = function (searchElement, fromIndex) { var k; if (null == this) throw new TypeError('"this" is null or not defined'); var o = Object(this), len = o.length >>> 0; if (0 === len) return -1; var n = +fromIndex || 0; if (Math.abs(n) === 1 / 0 && (n = 0), n >= len) return -1; for (k = Math.max(n >= 0 ? n : len - Math.abs(n), 0); k < len;) { if (k in o && o[k] === searchElement) return k; k++ } return -1 });
/**
 * Array.lastIndexOf - from: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/lastIndexOf
 */
Array.prototype.lastIndexOf || (Array.prototype.lastIndexOf = function (searchElement) { "use strict"; if (void 0 === this || null === this) throw new TypeError; var n, k, t = Object(this), len = t.length >>> 0; if (0 === len) return -1; for (n = len - 1, arguments.length > 1 && (n = Number(arguments[1]), n != n ? n = 0 : 0 != n && n != 1 / 0 && n != -(1 / 0) && (n = (n > 0 || -1) * Math.floor(Math.abs(n)))), k = n >= 0 ? Math.min(n, len - 1) : len - Math.abs(n); k >= 0; k--)if (k in t && t[k] === searchElement) return k; return -1 });
/**
 * Array.map - from: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map
 */
Array.prototype.map || (Array.prototype.map = function (callback, thisArg) { var T, A, k; if (null == this) throw new TypeError(" this is null or not defined"); var O = Object(this), len = O.length >>> 0; if ("function" != typeof callback) throw new TypeError(callback + " is not a function"); for (arguments.length > 1 && (T = thisArg), A = new Array(len), k = 0; k < len;) { var kValue, mappedValue; k in O && (kValue = O[k], mappedValue = callback.call(T, kValue, k, O), A[k] = mappedValue), k++ } return A });
/**
 * Array.reduce - from: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce
 */
Array.prototype.reduce || (Array.prototype.reduce = function (callback) { "use strict"; if (null == this) throw new TypeError("Array.prototype.reduce called on null or undefined"); if ("function" != typeof callback) throw new TypeError(callback + " is not a function"); var value, t = Object(this), len = t.length >>> 0, k = 0; if (2 == arguments.length) value = arguments[1]; else { for (; k < len && !(k in t);)k++; if (k >= len) throw new TypeError("Reduce of empty array with no initial value"); value = t[k++] } for (; k < len; k++)k in t && (value = callback(value, t[k], k, t)); return value });
/**
 * Array.some - from: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/some
 */
Array.prototype.some || (Array.prototype.some = function (fun) { "use strict"; if (null == this) throw new TypeError("Array.prototype.some called on null or undefined"); if ("function" != typeof fun) throw new TypeError; for (var t = Object(this), len = t.length >>> 0, thisArg = arguments.length >= 2 ? arguments[1] : void 0, i = 0; i < len; i++)if (i in t && fun.call(thisArg, t[i], i, t)) return !0; return !1 });
/* jshint ignore:end */
if (typeof ($) === 'undefined') {
	$ = {};
}

$.init = {
	// Evaluate a file and catch the exception.
	evalFile: function (path) {
		try {
			$.evalFile(path);
		} catch (e) { alert('Exception:' + e); }
	},
	// Evaluate all the files in the given folder
	evalFiles: function (jsxFolderPath) {
		var folder = new Folder(jsxFolderPath);
		if (folder.exists) {
			var jsxFiles = folder.getFiles('*.jsx');
			for (var i = 0, len = jsxFiles.length; i < len; i++) {
				var jsxFile = jsxFiles[i];
				$.init.evalFile(jsxFile);
			}
		}
	}
};

// var obj = {
// 	name: "<%= appName %>",
// 	message: "Hello from Gizmo!"
// };

// $.getExampleObject = JSON.stringify(obj);


// enable double clicking from the Macintosh Finder or the Windows Explorer
#target photoshop

// debug level: 0-2 (0:disable, 1:break on error, 2:break at beginning)
// $.level = 0;
// debugger; // launch debugger on next line

// on localized builds we pull the $$$/Strings from a .dat file, see documentation for more details
$.localize = true;

//=================================================================
// Globals
//=================================================================

// UI strings to be localized
var strTitle = localize("$$$/JavaScripts/ExportLayersToFiles/Title=Export Layers To Files");
var strAlertDocumentMustBeOpened = localize("$$$/JavaScripts/ExportLayersToFiles/OneDocument=You must have a document open to export!");
var strAlertNeedMultipleLayers = localize("$$$/JavaScripts/ExportLayersToFiles/NoLayers=You need a document with multiple layers to export!");
var strAlertWasSuccessful = localize("$$$/JavaScripts/ExportLayersToFiles/Success= was successful.");
var strUnexpectedError = localize("$$$/JavaScripts/ExportLayersToFiles/Unexpected=Unexpected error");
var strMessage = localize("$$$/JavaScripts/ExportLayersToFiles/Message=Export Layers To Files action settings");

// the drop down list indexes for file type
var FILE_TYPE = {
	bmpIndex: 0,
	jpegIndex: 1,
	pdfIndex: 2,
	psdIndex: 3,
	targaIndex: 4,
	tiffIndex: 5,
	png8Index: 6,
	png24Index: 7
}

var documentNames = [];
var texturePackerList = [];
var lang = "";
///////////////////////////////////////////////////////////////////////////////
// Dispatch
///////////////////////////////////////////////////////////////////////////////


main();



///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
// Function: main
// Usage: the core routine for this script
// Input: <none>
// Return: <none>
///////////////////////////////////////////////////////////////////////////////
function main() {
	if (app.documents.length <= 0) {
		if (DialogModes.NO != app.playbackDisplayDialogs) {
			alert(strAlertDocumentMustBeOpened);
		}
		return 'cancel'; // quit, returning 'cancel' (dont localize) makes the actions palette not record our script
	}

	var exportInfo = new Object();

	initExportInfo(exportInfo);
	// see if I am getting descriptor parameters
	descriptorToObject(exportInfo, app.playbackParameters, strMessage, postProcessExportInfo);

	try {
		for (var i = 0; i < app.documents.length; i++) {
			documentNames.push(app.documents[i].name);
		}

		for (var i = 0; i < documentNames.length; i++) {
			//var docName = app.activeDocument.name;  // save the app.activeDocument name before duplicate.
			var docName = documentNames[i];  // save the app.activeDocument name before duplicate.
			var layerCount = app.documents[docName].layers.length;
			var layerSetsCount = app.documents[docName].layerSets.length;
			var folderName = "/" + decodeURI(docName.substring(0, docName.indexOf(".")));
			var mainFolder = Folder(exportInfo.destination + folderName);
			var tmpName = folderName.split("_");
			lang = tmpName[tmpName.length - 1];

			//new document clear list array
			texturePackerList = [];
			if ((layerCount <= 1) && (layerSetsCount <= 0)) {
				if (DialogModes.NO != app.playbackDisplayDialogs) {
					alert(strAlertNeedMultipleLayers);
					return 'cancel'; // quit, returning 'cancel' (dont localize) makes the actions palette not record our script
				}
			} else {

				var rememberMaximize;
				var needMaximize = exportInfo.psdMaxComp ? QueryStateType.ALWAYS : QueryStateType.NEVER;
				if (exportInfo.fileType == FILE_TYPE.psdIndex && app.preferences.maximizeCompatibility != needMaximize) {
					rememberMaximize = app.preferences.maximizeCompatibility;
					app.preferences.maximizeCompatibility = needMaximize;
				}

				app.activeDocument = app.documents[docName];
				var duppedDocument = app.activeDocument.duplicate();
				//set active layer is last layer
				duppedDocument.activeLayer = duppedDocument.layers[duppedDocument.layers.length - 1]; // for removing

				// Check if it exist, if not create it.
				if (!mainFolder.exists) {
					mainFolder.create();
				}
				// Enter Recursive Function
				// alert(folderName,"fisrt folderName");
				exportChildren(duppedDocument, exportInfo, duppedDocument, exportInfo.fileNamePrefix, folderName);
				duppedDocument.close(SaveOptions.DONOTSAVECHANGES);
			}
			var fileName = decodeURI(docName.substring(0, docName.indexOf("."))) + ".txt";
			//write texture packer folder
			writeTextFile(exportInfo.destination, fileName, texturePackerList, i == documentNames.length - 1);
		}

		var d = objectToDescriptor(exportInfo, strMessage, preProcessExportInfo);
		app.putCustomOptions("4d633fbb-ed90-480d-8e03-cccb16131a34", d);

		var dd = objectToDescriptor(exportInfo, strMessage, preProcessExportInfo);
		app.playbackParameters = dd;

		if (rememberMaximize != undefined) {
			app.preferences.maximizeCompatibility = rememberMaximize;
		}

		if (DialogModes.ALL == app.playbackDisplayDialogs) {
			alert(strTitle + strAlertWasSuccessful);
		}

		app.playbackDisplayDialogs = DialogModes.ALL;
	} catch (e) {
		if (DialogModes.NO != app.playbackDisplayDialogs) {
			alert(e);
		}
		return 'cancel'; // quit, returning 'cancel' (dont localize) makes the actions palette not record our script
	}
}


///////////////////////////////////////////////////////////////////////////////
// Function: initExportInfo
// Usage: create our default parameters
// Input: a new Object
// Return: a new object with params set to default
///////////////////////////////////////////////////////////////////////////////
function initExportInfo(exportInfo) {
	var file = new File((new File($.fileName)).parent + "/config.json");
	file.open("r");
	var conf = JSON.parse(file.read());
	exportInfo.destination = new String("");
	exportInfo.fileNamePrefix = conf.photoshopScript.fileNamePrefix;
	exportInfo.visibleOnly = conf.photoshopScript.visibleOnly;
	exportInfo.fileType = FILE_TYPE[conf.photoshopScript.fileType];
	exportInfo.icc = true;
	exportInfo.jpegQuality = 8;
	exportInfo.psdMaxComp = true;
	exportInfo.tiffCompression = TIFFEncoding.NONE;
	exportInfo.tiffJpegQuality = 8;
	exportInfo.pdfEncoding = PDFEncoding.JPEG;
	exportInfo.pdfJpegQuality = 8;
	exportInfo.targaDepth = TargaBitsPerPixels.TWENTYFOUR;
	exportInfo.bmpDepth = BMPDepthType.TWENTYFOUR;
	exportInfo.png24Transparency = true;
	exportInfo.png24Interlaced = false;
	exportInfo.png24Trim = true;
	exportInfo.png8Transparency = true;
	exportInfo.png8Interlaced = false;
	exportInfo.png8Trim = true;

	try {
		//exportInfo.destination = Folder(app.activeDocument.fullName.parent).fsName; // destination folder
		exportInfo.destination = Folder($.fileName).parent.fsName;
		// var tmp = app.activeDocument.fullName.name;
		// exportInfo.fileNamePrefix = decodeURI(tmp.substring(0, tmp.indexOf("."))); // filename body part
	} catch (someError) {
		exportInfo.destination = new String("");
		exportInfo.fileNamePrefix = app.activeDocument.name; // filename body part
	}
}


///////////////////////////////////////////////////////////////////////////////
// Function: saveFile
// Usage: the worker routine, take our params and save the file accordingly
// Input: reference to the document, the name of the output file, 
//        export info object containing more information
// Return: <none>, a file on disk
///////////////////////////////////////////////////////////////////////////////
function saveFile(docRef, fileNameBody, exportInfo, folderPath) {
	switch (exportInfo.fileType) {
		case FILE_TYPE.jpegIndex:
			docRef.bitsPerChannel = BitsPerChannelType.EIGHT;
			var saveFile = new File(exportInfo.destination + "/" + fileNameBody + ".jpg");
			jpgSaveOptions = new JPEGSaveOptions();
			jpgSaveOptions.embedColorProfile = exportInfo.icc;
			jpgSaveOptions.quality = exportInfo.jpegQuality;
			docRef.saveAs(saveFile, jpgSaveOptions, true, Extension.LOWERCASE);
			break;
		case FILE_TYPE.psdIndex:
			var saveFile = new File(exportInfo.destination + "/" + fileNameBody + ".psd");
			psdSaveOptions = new PhotoshopSaveOptions();
			psdSaveOptions.embedColorProfile = exportInfo.icc;
			psdSaveOptions.maximizeCompatibility = exportInfo.psdMaxComp;
			docRef.saveAs(saveFile, psdSaveOptions, true, Extension.LOWERCASE);
			break;
		case FILE_TYPE.tiffIndex:
			var saveFile = new File(exportInfo.destination + "/" + fileNameBody + ".tif");
			tiffSaveOptions = new TiffSaveOptions();
			tiffSaveOptions.embedColorProfile = exportInfo.icc;
			tiffSaveOptions.imageCompression = exportInfo.tiffCompression;
			if (TIFFEncoding.JPEG == exportInfo.tiffCompression) {
				tiffSaveOptions.jpegQuality = exportInfo.tiffJpegQuality;
			}
			docRef.saveAs(saveFile, tiffSaveOptions, true, Extension.LOWERCASE);
			break;
		case FILE_TYPE.pdfIndex:
			if (docRef.bitsPerChannel == BitsPerChannelType.THIRTYTWO)
				docRef.bitsPerChannel = BitsPerChannelType.SIXTEEN;
			var saveFile = new File(exportInfo.destination + "/" + fileNameBody + ".pdf");
			pdfSaveOptions = new PDFSaveOptions();
			pdfSaveOptions.embedColorProfile = exportInfo.icc;
			pdfSaveOptions.encoding = exportInfo.pdfEncoding;
			if (PDFEncoding.JPEG == exportInfo.pdfEncoding) {
				pdfSaveOptions.jpegQuality = exportInfo.pdfJpegQuality;
			}
			docRef.saveAs(saveFile, pdfSaveOptions, true, Extension.LOWERCASE);
			break;
		case FILE_TYPE.targaIndex:
			docRef.bitsPerChannel = BitsPerChannelType.EIGHT;
			var saveFile = new File(exportInfo.destination + "/" + fileNameBody + ".tga");
			targaSaveOptions = new TargaSaveOptions();
			targaSaveOptions.resolution = exportInfo.targaDepth;
			docRef.saveAs(saveFile, targaSaveOptions, true, Extension.LOWERCASE);
			break;
		case FILE_TYPE.bmpIndex:
			docRef.bitsPerChannel = BitsPerChannelType.EIGHT;
			var saveFile = new File(exportInfo.destination + "/" + fileNameBody + ".bmp");
			bmpSaveOptions = new BMPSaveOptions();
			bmpSaveOptions.depth = exportInfo.bmpDepth;
			docRef.saveAs(saveFile, bmpSaveOptions, true, Extension.LOWERCASE);
			break;
		case FILE_TYPE.png8Index:
			saveFile(docRef, fileNameBody, exportInfo, false, true);
			function saveFile(docRef, fileNameBody, exportInfo, interlacedValue, transparencyValue) {
				var id5 = charIDToTypeID("Expr");
				var desc3 = new ActionDescriptor();
				var id6 = charIDToTypeID("Usng");
				var desc4 = new ActionDescriptor();
				var id7 = charIDToTypeID("Op  ");
				var id8 = charIDToTypeID("SWOp");
				var id9 = charIDToTypeID("OpSa");
				desc4.putEnumerated(id7, id8, id9);
				var id10 = charIDToTypeID("Fmt ");
				var id11 = charIDToTypeID("IRFm");
				var id12 = charIDToTypeID("PNG8");
				desc4.putEnumerated(id10, id11, id12);
				var id13 = charIDToTypeID("Intr"); //Interlaced
				desc4.putBoolean(id13, interlacedValue);
				var id14 = charIDToTypeID("RedA");
				var id15 = charIDToTypeID("IRRd");
				var id16 = charIDToTypeID("Prcp"); //Algorithm
				desc4.putEnumerated(id14, id15, id16);
				var id17 = charIDToTypeID("RChT");
				desc4.putBoolean(id17, false);
				var id18 = charIDToTypeID("RChV");
				desc4.putBoolean(id18, false);
				var id19 = charIDToTypeID("AuRd");
				desc4.putBoolean(id19, false);
				var id20 = charIDToTypeID("NCol"); //NO. Of Colors
				desc4.putInteger(id20, 256);
				var id21 = charIDToTypeID("Dthr"); //Dither
				var id22 = charIDToTypeID("IRDt");
				var id23 = charIDToTypeID("Dfsn"); //Dither type
				desc4.putEnumerated(id21, id22, id23);
				var id24 = charIDToTypeID("DthA");
				desc4.putInteger(id24, 100);
				var id25 = charIDToTypeID("DChS");
				desc4.putInteger(id25, 0);
				var id26 = charIDToTypeID("DCUI");
				desc4.putInteger(id26, 0);
				var id27 = charIDToTypeID("DChT");
				desc4.putBoolean(id27, false);
				var id28 = charIDToTypeID("DChV");
				desc4.putBoolean(id28, false);
				var id29 = charIDToTypeID("WebS");
				desc4.putInteger(id29, 0);
				var id30 = charIDToTypeID("TDth"); //transparency dither
				var id31 = charIDToTypeID("IRDt");
				var id32 = charIDToTypeID("None");
				desc4.putEnumerated(id30, id31, id32);
				var id33 = charIDToTypeID("TDtA");
				desc4.putInteger(id33, 100);
				var id34 = charIDToTypeID("Trns"); //Transparency
				desc4.putBoolean(id34, transparencyValue);
				var id35 = charIDToTypeID("Mtt ");
				desc4.putBoolean(id35, true);		 //matte
				var id36 = charIDToTypeID("MttR"); //matte color
				desc4.putInteger(id36, 255);
				var id37 = charIDToTypeID("MttG");
				desc4.putInteger(id37, 255);
				var id38 = charIDToTypeID("MttB");
				desc4.putInteger(id38, 255);
				var id39 = charIDToTypeID("SHTM");
				desc4.putBoolean(id39, false);
				var id40 = charIDToTypeID("SImg");
				desc4.putBoolean(id40, true);
				var id41 = charIDToTypeID("SSSO");
				desc4.putBoolean(id41, false);
				var id42 = charIDToTypeID("SSLt");
				var list1 = new ActionList();
				desc4.putList(id42, list1);
				var id43 = charIDToTypeID("DIDr");
				desc4.putBoolean(id43, false);
				var id44 = charIDToTypeID("In  ");
				desc4.putPath(id44, new File(exportInfo.destination + "/" + folderPath + "/" + fileNameBody + ".png"));
				var id45 = stringIDToTypeID("SaveForWeb");
				desc3.putObject(id6, id45, desc4);
				executeAction(id5, desc3, DialogModes.NO);
			}
			//var saveFile = new File(exportInfo.destination + "/" + fileNameBody + ".png");
			//bmpSaveOptions = new BMPSaveOptions();
			//bmpSaveOptions.depth = exportInfo.bmpDepth;
			//docRef.saveAs(saveFile, bmpSaveOptions, true, Extension.LOWERCASE);
			break;
		case FILE_TYPE.png24Index:
			saveFile(docRef, fileNameBody, exportInfo, false, true);
			function saveFile(docRef, fileNameBody, exportInfo, interlacedValue, transparencyValue) {
				var id6 = charIDToTypeID("Expr");
				var desc3 = new ActionDescriptor();
				var id7 = charIDToTypeID("Usng");
				var desc4 = new ActionDescriptor();
				var id8 = charIDToTypeID("Op  ");
				var id9 = charIDToTypeID("SWOp");
				var id10 = charIDToTypeID("OpSa");
				desc4.putEnumerated(id8, id9, id10);
				var id11 = charIDToTypeID("Fmt ");
				var id12 = charIDToTypeID("IRFm");
				var id13 = charIDToTypeID("PN24");
				desc4.putEnumerated(id11, id12, id13);
				var id14 = charIDToTypeID("Intr");
				desc4.putBoolean(id14, interlacedValue);
				var id15 = charIDToTypeID("Trns");
				desc4.putBoolean(id15, transparencyValue);
				var id16 = charIDToTypeID("Mtt ");
				desc4.putBoolean(id16, true);
				var id17 = charIDToTypeID("MttR");
				desc4.putInteger(id17, 255);
				var id18 = charIDToTypeID("MttG");
				desc4.putInteger(id18, 255);
				var id19 = charIDToTypeID("MttB");
				desc4.putInteger(id19, 255);
				var id20 = charIDToTypeID("SHTM");
				desc4.putBoolean(id20, false);
				var id21 = charIDToTypeID("SImg");
				desc4.putBoolean(id21, true);
				var id22 = charIDToTypeID("SSSO");
				desc4.putBoolean(id22, false);
				var id23 = charIDToTypeID("SSLt");
				var list1 = new ActionList();
				desc4.putList(id23, list1);
				var id24 = charIDToTypeID("DIDr");
				desc4.putBoolean(id24, false);
				var id25 = charIDToTypeID("In  ");
				desc4.putPath(id25, new File(exportInfo.destination + "/" + folderPath + "/" + fileNameBody + ".png"));
				var id26 = stringIDToTypeID("SaveForWeb");
				desc3.putObject(id7, id26, desc4);
				executeAction(id6, desc3, DialogModes.NO);
			}

			//var saveFile = new File(exportInfo.destination + "/" + fileNameBody + ".png");
			//bmpSaveOptions = new BMPSaveOptions();
			//bmpSaveOptions.depth = exportInfo.bmpDepth;
			//docRef.saveAs(saveFile, bmpSaveOptions, true, Extension.LOWERCASE);
			break;
		default:
			if (DialogModes.NO != app.playbackDisplayDialogs) {
				alert(strUnexpectedError);
			}
			break;
	}
}


///////////////////////////////////////////////////////////////////////////////
// Function: zeroSuppress
// Usage: return a string padded to digit(s)
// Input: num to convert, digit count needed
// Return: string padded to digit length
///////////////////////////////////////////////////////////////////////////////
function zeroSuppress(num, digit) {
	var tmp = num.toString();
	while (tmp.length < digit) {
		tmp = "0" + tmp;
	}
	return tmp;
}

function saveDuplicate(exportInfo, item, fileNameBody, duppedDocumentTmp, folderPath) {
	item.duplicate(duppedDocumentTmp);
	app.activeDocument = duppedDocumentTmp;
	if ((FILE_TYPE.psdIndex == exportInfo.fileType) || (FILE_TYPE.png24Index == exportInfo.fileType) || (FILE_TYPE.png8Index == exportInfo.fileType)) { // PSD: Keep transparency
		app.activeDocument.trim(TrimType.TRANSPARENT);
	} else { // just flatten
		duppedDocumentTmp.flatten();
	}
	fileNameBody = fileNameBody.replace(/[:\/\\*\?\"\<\>\|]/g, "_");  // '/\:*?"<>|' -> '_'
	if (fileNameBody.length > 120) {
		fileNameBody = fileNameBody.substring(0, 120);
	}
	saveFile(duppedDocumentTmp, fileNameBody, exportInfo, folderPath);
	duppedDocumentTmp.close(SaveOptions.DONOTSAVECHANGES);
}

///////////////////////////////////////////////////////////////////////////////
// Function: exportChildren
// Usage: find all the children in this document to save
// Input: duplicate document, original document, export info,
//        reference to document, starting file name
// Return: <none>, documents are saved accordingly
///////////////////////////////////////////////////////////////////////////////
function exportChildren(dupObj, exportInfo, dupDocRef, fileNamePrefix, folderPath, folderName) {
	var mainfolder = Folder(exportInfo.destination + folderPath);
	// Check if it exist, if not create it.
	if (!mainfolder.exists) {
		mainfolder.create();
	}

	for (var i = 0; i < dupObj.artLayers.length; i++) {
		var item = dupObj.artLayers[i];
		if (exportInfo.visibleOnly) { // visible layer only
			if (!item.visible) {
				continue;
			}
			if (item.name.indexOf("!") != -1) {
				continue;
			}
		}

		// 檢查圖片多語系
		var name = item.name.split("_");
		if (name[0] === "LANG") {
			if (name[name.length - 1].toLowerCase() !== lang.toLowerCase()) {
				continue;
			}
			name.splice(name.length - 1, 1);
			item.name = name.join("_");
		}

		item.visible = true;
		try {
			var isMultlang = false;
			if (item.kind == LayerKind.TEXT) {
				for (var j = 0, jMax = dupObj.layerSets.length; j < jMax; j++) {
					var folder = dupObj.layerSets[j];
					if (folder.name.toLowerCase() == item.name.toLowerCase()) {
						for (var k = 0, kMax = folder.artLayers.length; k < kMax; k++) {
							var duppedDocumentTmp = documents.add(dupDocRef.width, dupDocRef.height, dupDocRef.resolution,
								'exportedLayer' + i, NewDocumentMode.RGB,
								DocumentFill.TRANSPARENT);
							app.activeDocument = dupDocRef;
							var artLayer = folder.artLayers[k];
							var artLayerNameTokens = artLayer.name.split("_");
							var btnState = artLayerNameTokens.slice(-1).pop();
							btnState = String(btnState).toUpperCase();
							var fileNameBody = fileNamePrefix + item.name + "_" + btnState;
							artLayer.duplicate(duppedDocumentTmp);
							saveDuplicate(exportInfo, item, fileNameBody, duppedDocumentTmp, folderPath);
						}
						folder.visible = false;
						isMultlang = true;
						break;
					}
				}
			}

			if (!isMultlang) {
				var duppedDocumentTmp = documents.add(dupDocRef.width, dupDocRef.height, dupDocRef.resolution,
					'exportedLayer' + i, NewDocumentMode.RGB,
					DocumentFill.TRANSPARENT);
				app.activeDocument = dupDocRef;
				var fileNameBody = fileNamePrefix + item.name;
				saveDuplicate(exportInfo, item, fileNameBody, duppedDocumentTmp, folderPath);
			}
		}
		catch (e) {
			alert(e);
			dupDocRef.close(SaveOptions.DONOTSAVECHANGES);
			duppedDocumentTmp.close(SaveOptions.DONOTSAVECHANGES);
			return;
		}
	}
	//artLayers have image
	if (dupObj.artLayers.length > 0) {
		texturePackerList.push(folderName);
	}

	//next folder
	for (var i = 0; i < dupObj.layerSets.length; i++) {
		if (exportInfo.visibleOnly) { // visible layer only
			if (!dupObj.layerSets[i].visible) {
				continue;
			}
		}
		exportChildren(dupObj.layerSets[i], exportInfo, dupDocRef, fileNamePrefix, (folderPath + "/" + dupObj.layerSets[i].name).replace(/\//g, "\\"), dupObj.layerSets[i].name);  // recursive call
	}
}


///////////////////////////////////////////////////////////////////////////////
// Function: objectToDescriptor
// Usage: create an ActionDescriptor from a JavaScript Object
// Input: JavaScript Object (o)
//        object unique string (s)
//        Pre process converter (f)
// Return: ActionDescriptor
// NOTE: Only boolean, string, number and UnitValue are supported, use a pre processor
//       to convert (f) other types to one of these forms.
// REUSE: This routine is used in other scripts. Please update those if you 
//        modify. I am not using include or eval statements as I want these 
//        scripts self contained.
///////////////////////////////////////////////////////////////////////////////
function objectToDescriptor(o, s, f) {
	if (undefined != f) {
		o = f(o);
	}
	var d = new ActionDescriptor;
	var l = o.reflect.properties.length;
	d.putString(app.charIDToTypeID('Msge'), s);
	for (var i = 0; i < l; i++) {
		var k = o.reflect.properties[i].toString();
		if (k == "__proto__" || k == "__count__" || k == "__class__" || k == "reflect")
			continue;
		var v = o[k];
		k = app.stringIDToTypeID(k);
		switch (typeof (v)) {
			case "boolean":
				d.putBoolean(k, v);
				break;
			case "string":
				d.putString(k, v);
				break;
			case "number":
				d.putDouble(k, v);
				break;
			default:
				{
					if (v instanceof UnitValue) {
						var uc = new Object;
						uc["px"] = charIDToTypeID("#Rlt"); // unitDistance
						uc["%"] = charIDToTypeID("#Prc"); // unitPercent
						d.putUnitDouble(k, uc[v.type], v.value);
					} else {
						throw (new Error("Unsupported type in objectToDescriptor " + typeof (v)));
					}
				}
		}
	}
	return d;
}


///////////////////////////////////////////////////////////////////////////////
// Function: descriptorToObject
// Usage: update a JavaScript Object from an ActionDescriptor
// Input: JavaScript Object (o), current object to update (output)
//        Photoshop ActionDescriptor (d), descriptor to pull new params for object from
//        object unique string (s)
//        JavaScript Function (f), post process converter utility to convert
// Return: Nothing, update is applied to passed in JavaScript Object (o)
// NOTE: Only boolean, string, number and UnitValue are supported, use a post processor
//       to convert (f) other types to one of these forms.
// REUSE: This routine is used in other scripts. Please update those if you 
//        modify. I am not using include or eval statements as I want these 
//        scripts self contained.
///////////////////////////////////////////////////////////////////////////////
function descriptorToObject(o, d, s, f) {
	var l = d.count;
	if (l) {
		var keyMessage = app.charIDToTypeID('Msge');
		if (d.hasKey(keyMessage) && (s != d.getString(keyMessage))) return;
	}
	for (var i = 0; i < l; i++) {
		var k = d.getKey(i); // i + 1 ?
		var t = d.getType(k);
		strk = app.typeIDToStringID(k);
		switch (t) {
			case DescValueType.BOOLEANTYPE:
				o[strk] = d.getBoolean(k);
				break;
			case DescValueType.STRINGTYPE:
				o[strk] = d.getString(k);
				break;
			case DescValueType.DOUBLETYPE:
				o[strk] = d.getDouble(k);
				break;
			case DescValueType.UNITDOUBLE:
				{
					var uc = new Object;
					uc[charIDToTypeID("#Rlt")] = "px"; // unitDistance
					uc[charIDToTypeID("#Prc")] = "%"; // unitPercent
					uc[charIDToTypeID("#Pxl")] = "px"; // unitPixels
					var ut = d.getUnitDoubleType(k);
					var uv = d.getUnitDoubleValue(k);
					o[strk] = new UnitValue(uv, uc[ut]);
				}
				break;
			case DescValueType.INTEGERTYPE:
			case DescValueType.ALIASTYPE:
			case DescValueType.CLASSTYPE:
			case DescValueType.ENUMERATEDTYPE:
			case DescValueType.LISTTYPE:
			case DescValueType.OBJECTTYPE:
			case DescValueType.RAWTYPE:
			case DescValueType.REFERENCETYPE:
			default:
				throw (new Error("Unsupported type in descriptorToObject " + t));
		}
	}
	if (undefined != f) {
		o = f(o);
	}
}


///////////////////////////////////////////////////////////////////////////////
// Function: preProcessExportInfo
// Usage: convert Photoshop enums to strings for storage
// Input: JavaScript Object of my params for this script
// Return: JavaScript Object with objects converted for storage
///////////////////////////////////////////////////////////////////////////////
function preProcessExportInfo(o) {
	o.tiffCompression = o.tiffCompression.toString();
	o.pdfEncoding = o.pdfEncoding.toString();
	o.targaDepth = o.targaDepth.toString();
	o.bmpDepth = o.bmpDepth.toString();
	return o;
}

///////////////////////////////////////////////////////////////////////////////
// Function: postProcessExportInfo
// Usage: convert strings from storage to Photoshop enums
// Input: JavaScript Object of my params in string form
// Return: JavaScript Object with objects in enum form
///////////////////////////////////////////////////////////////////////////////
function postProcessExportInfo(o) {
	o.tiffCompression = eval(o.tiffCompression);
	o.pdfEncoding = eval(o.pdfEncoding);
	o.targaDepth = eval(o.targaDepth);
	o.bmpDepth = eval(o.bmpDepth);
	return o;
}


function writeAllLayersPosition() {
	app.preferences.rulerUnits = Units.PIXELS;
	var obj = app.activeDocument;
	var layers = obj.artLayers;
	var positions = [];
	for (var i = 0; i < layers.length; i++) {
		var layer = layers[i];
		if (!layer.visible) continue;
		var x = layer.bounds[0];
		var y = layer.bounds[1];
		var n = layer.name;
		var str = "\t<image url='" + n + "' x='" + x + "' y='" + y + "' />";
		positions.push(str);
	}
	var textfield = layers.add();
	textfield.kind = LayerKind.TEXT;
	textfield.textItem.contents = positions.join("\r");
};

function writeTextFile(path, fileName, data, isFinish) {
	// 輸出 CSS 定位檔
	// var filePath = File.saveDialog("file name","*.json");
	// if(!filePath){
	//     alert("建立開啟檔案！！");
	// 	return; 
	// }

	// if(filePath.exists && !confirm("你確定要覆蓋這個檔案？")){
	//     return;
	// }

	// 開啟檔案
	var filePoint = File(path + "/packerList/" + fileName);
	if (!filePoint.open("w", "", "")) {
		alert("無法開啟檔案！！");
		return;
	}

	for (var i = 0, iMax = data.length; i < iMax; i++) {
		var item = data[i];
		filePoint.writeln(item);
	}

	if (isFinish) {
		filePoint.writeln("#end");
	}
	/* 以行為單位寫入檔案 */
	//filePoint.writeln(data);
	// --or--
	/* 寫入內容 */
	//filePoint.write("content");


	/*關閉檔案*/
	filePoint.close();
}


// End Export Layers To Files.jsx
