Set up the environment:
1. Check the Node Version to be 8.00 or higher.
2. Open the Photoshop:
    2.1: Goto Edit -> Preference -> Plug-In
    2.2: Disable the "Enable Generator" checkbox
    2.3: Enable the "Enable Remote Connections" checkbox
    2.4: Change the password to "password" without the quotes
    2.5: Restart Photoshop
    2.6: More Info in the official website https://github.com/adobe-photoshop/generator-core/wiki/Generator-Development-Environment-Setup


FIXME List:
1.The folder name in file.psd can not contain space. Will make cmd TexturePacker --data error. 

Run CMD to execute:
node ./psd.js --publish